import {Component, OnInit} from '@angular/core';
import {PageType} from "../shared/enum/page-type.enum";
import {EndpointDTO} from "../shared/dtos/endpoint.dto";
import {DataType, Measurement} from "../shared/units";
import {MatDialog} from "@angular/material/dialog";
import {DataSelectionDialogComponent} from "./data-selection/data-selection-dialog.component";
import {AvailableEndpointsService} from "../home-page/sevices/available-endpoints.service";
import {ConfigDataDTO} from "../home-page/dtos/config-data.dto";
import {HttpClient, HttpErrorResponse, HttpStatusCode} from "@angular/common/http";
import {DataConverterService} from "./services/data-converter.service";
import {InactiveEndpointsRepository} from "./services/inactive-endpoints.repository";
import {WarningPopupComponent} from "./warning-popup/warning-popup.component";
import {catchError, throwError} from "rxjs";
import {DataNameParserService} from "../shared/services/data-name-parser.service";

@Component({
  selector: 'app-recent-datasets',
  templateUrl: './recent-datasets.component.html',
  styleUrls: ['./recent-datasets.component.css']
})

export class RecentDatasetsComponent implements OnInit {
  pageOptions = PageType;
  displayedColumns: string[] = ['checkbox', 'id'];
  configData: ConfigDataDTO;
  dataTypes: DataType[];
  selectedUnits: Map<string, string> = new Map<string, string>();
  endpoints: EndpointDTO[];
  selections: EndpointDTO[] = [];
  error: HttpErrorResponse;
  preparingData: boolean = true;
  noData: boolean;
  errorMessage: string;
  cols: number;
  rowHeight: string;
  readonly noDataText = "OOPS...!\n" +
    "Looks like you haven't uploaded any configuration file yet.";
  readonly reload = "Reload";
  readonly goToHomePage = "Go to home page";

  constructor(private readonly dialog: MatDialog,
              private readonly availableEndpointsService: AvailableEndpointsService,
              private readonly http: HttpClient,
              private readonly dataConverterService: DataConverterService,
              private readonly inactiveEndpointsRepository: InactiveEndpointsRepository,
              readonly dataNameParserService: DataNameParserService) {
  }

  ngOnInit(): void {
    this.inactiveEndpointsRepository.getInactiveEndpoints().pipe(
      catchError(err => {
        if (this.configData) {
          this.error = err;
          this.setErrorMessage(this.error.status);
          return throwError(err);
        }
        return null;
      })
    )
      .subscribe(inactiveEndpoints => {
        if (inactiveEndpoints.size > 0 && this.configData) {
          const dialogRef = this.dialog.open(WarningPopupComponent, {
            data: {inactiveEndpoints: inactiveEndpoints}
          });
        }
      });
    this.availableEndpointsService.configData$.pipe(
      catchError(err => {
        this.error = err;
        this.setErrorMessage(this.error.status);
        return throwError(err);
      })
    )
      .subscribe(configData => {
        this.configData = configData;
        this.prepareRecentData();
        setInterval(() => this.prepareRecentData(), 1800000);
      });
  }

  prepareRecentData() {
    this.preparingData = true;
    setTimeout(() => {
      if (!this.configData) {
        this.noData = true;
        this.preparingData = false;
      } else {
        this.displayedColumns = ['checkbox', 'id'];
        this.endpoints = [];
        this.dataTypes = [];
        this.selections = [];
        this.prepareDataTypes();
        this.prepareEndpoints();
      }
    }, 2000);
  }

  private prepareEndpoints(): void {
    const recentDataEndpoint = "http://localhost:8080/recent";
    this.http.get(recentDataEndpoint).subscribe(response => {
        this.endpoints = this.mapToEndpoints(response);
      },
      err => {
        this.endpoints = [];
        this.error = err;
        this.setErrorMessage(this.error.status);
      },
      () => this.preparingData = false);
  }

  private mapToEndpoints(response: any): EndpointDTO[] {
    return response.data.map((item: any) => {
      const measurements = item.data.map((measurement: { name: string, value: number }) => {
        if (isNaN(measurement.value)) {
          return new Measurement(measurement.name, measurement.value, this.selectedUnits.get(measurement.name));
        }
        if (!measurement.value) {
          return new Measurement(measurement.name, '-', this.selectedUnits.get(measurement.name));
        }
        return new Measurement(measurement.name, Number(Number(measurement.value).toFixed(2)), this.selectedUnits.get(measurement.name));
      });
      const name = this.configData.stations.filter(station => station.endpoint === item.name)[0].name;
      return {
        id: name,
        measurements: measurements
      } as EndpointDTO;
    })
  }

  private prepareDataTypes(): void {
    this.dataTypes = this.configData.sensors.map(sensor => {
      return {
        name: sensor.name,
        units: sensor.possibleUnits,
        currentUnit: sensor.defaultUnit
      } as DataType;
    });
    this.dataTypes.forEach(dataType => {
      this.displayedColumns.push(dataType.name);
      this.selectedUnits.set(dataType.name, dataType.currentUnit);
    });
    this.cols = Math.ceil(this.dataTypes.length / 5);
    this.displayedColumns.push('button');
  }

  findMeasurement(m: Measurement[], s: string): number {
    var v = null;
    m.forEach(x => {
      if (x.getValueOfMeasurement(s) != null) {
        v = x.getValueOfMeasurement(s);
      }
    })
    return v;
  }

  openDatePickerDialog(endpoints: EndpointDTO[]): void {
    const dialogRef = this.dialog.open(DataSelectionDialogComponent, {
      data: {endpoints: endpoints, maxMonthRange: this.configData.maxMonthRange},
      disableClose: true
    });
  }

  unitChanged(field: DataType): void {
    this.selectedUnits.set(field.name, field.currentUnit);
    this.endpoints.map(endpoint =>
      endpoint.measurements.map(measurement =>
        measurement.name === field.name ? this.dataConverterService.convertUnitOnRecentData(measurement, field.currentUnit) : measurement));
  }

  updateSelections(endpoint: EndpointDTO): void {
    if (this.selections.includes(endpoint)) {
      const endpointIdx = this.selections.indexOf(endpoint);
      this.selections.splice(endpointIdx, endpointIdx + 1);
    } else {
      this.selections.push(endpoint);
    }
  }

  private setErrorMessage(status: HttpStatusCode): void {
    if (status === HttpStatusCode.InternalServerError) {
      this.errorMessage = "Sorry, an error has ocurred during data processing. Try again later."
    } else if (status === HttpStatusCode.ServiceUnavailable) {
      this.errorMessage = "Sorry, DataHub server is currently unavailable. Try again later."
    } else if (status === HttpStatusCode.NoContent) {
      this.errorMessage = "Sorry, there is currently no available data based on your configuration file."
    } else {
      this.errorMessage = "Sorry, an error has ocurred. Try again later."
    }
  }

  reloadPage(): void {
    this.ngOnInit();
  }
}
