import {Component, Inject, OnInit} from "@angular/core";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";

@Component({
  selector: 'app-warning-popup',
  templateUrl: './warning-popup.component.html',
  styleUrls: ['./warning-popup.component.scss']
})
export class WarningPopupComponent {

  inactiveEndpoints: Map<string, Date>;

  constructor(dialogRef: MatDialogRef<WarningPopupComponent>,
              @Inject(MAT_DIALOG_DATA) data: { inactiveEndpoints: Map<string, Date> }) {
    this.inactiveEndpoints = data.inactiveEndpoints;
  }
}
