import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from "@angular/router";
import {SharedModule} from "../shared/shared.module";
import {MatIconModule} from "@angular/material/icon";
import {MatButtonModule} from "@angular/material/button";
import {RecentDatasetsComponent} from "./recent-datasets.component";
import {MatTableModule} from "@angular/material/table";
import {MatCheckboxModule} from "@angular/material/checkbox";
import {MatNativeDateModule} from '@angular/material/core';
import {MatGridListModule} from "@angular/material/grid-list";
import {MatRadioModule} from "@angular/material/radio";
import {MatCardModule} from "@angular/material/card";
import {DataSelectionDialogComponent} from "./data-selection/data-selection-dialog.component";
import {MatDatepickerModule} from "@angular/material/datepicker";
import {MatFormFieldModule} from "@angular/material/form-field";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MatDialogModule} from "@angular/material/dialog";
import {MatSelectModule} from "@angular/material/select";
import {MatInputModule} from "@angular/material/input";
import {AvailableEndpointsService} from "../home-page/sevices/available-endpoints.service";
import {MatTooltipModule} from "@angular/material/tooltip";
import {ChartDataRepository} from "../charts-page/services/chart-data.repository";
import {DataConverterService} from "./services/data-converter.service";
import {MatProgressSpinnerModule} from "@angular/material/progress-spinner";
import {InactiveEndpointsRepository} from "./services/inactive-endpoints.repository";
import {WarningPopupComponent} from "./warning-popup/warning-popup.component";
import {MatListModule} from "@angular/material/list";
import {ChartCounterService} from "../shared/services/chart-counter.service";
import {DataNameParserService} from "../shared/services/data-name-parser.service";

@NgModule({
  declarations: [RecentDatasetsComponent, DataSelectionDialogComponent, WarningPopupComponent]
  ,
  imports: [
    RouterModule,
    SharedModule,
    MatIconModule,
    MatButtonModule,
    MatTableModule,
    MatCheckboxModule,
    CommonModule,
    MatGridListModule,
    MatRadioModule,
    MatCardModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatNativeDateModule,
    FormsModule,
    MatDialogModule,
    MatSelectModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatTooltipModule,
    MatProgressSpinnerModule,
    MatListModule
  ],
  providers: [
    AvailableEndpointsService,
    ChartDataRepository,
    DataConverterService,
    InactiveEndpointsRepository,
    ChartCounterService,
    DataNameParserService
  ],
  exports: [RecentDatasetsComponent, DataSelectionDialogComponent]
})
export class RecentDatasetsModule {
}
