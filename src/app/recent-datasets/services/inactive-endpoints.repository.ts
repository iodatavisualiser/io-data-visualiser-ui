import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {map, Observable} from "rxjs";

@Injectable()
export class InactiveEndpointsRepository {
  constructor(private readonly http: HttpClient) {
  }

  readonly inactiveDataEndpoint = "http://localhost:8080/inactive";

  public getInactiveEndpoints(): Observable<Map<string, Date>> {
    return this.http.get(this.inactiveDataEndpoint).pipe(map((response: any) => {
        const inactiveEndpointsMap = new Map();
        response.data.forEach((item: any) => {
          inactiveEndpointsMap.set(item.name, new Date(item.lastDate))
        })
        return inactiveEndpointsMap;
      })
    );
  }
}
