import {Injectable} from "@angular/core";
import {
  amperageDrawUnits,
  humidityUnits,
  lengthUnits,
  Measurement, particleConcentrationUnits,
  pmUnits,
  powerUnits,
  pressureUnits,
  temperatureUnits, timeUnits, voltageUnits
} from "../../shared/units";
import {MeasurementDTO} from "../../charts-page/dtos/measurement.dto";

@Injectable()
export class DataConverterService {
  temperatureUnits: string[] = Object.values(temperatureUnits);
  pressureUnits: string[] = Object.values(pressureUnits);
  pmUnits: string[] = Object.values(pmUnits);
  powerUnits: string[] = Object.values(powerUnits);
  lengthUnits: string[] = Object.values(lengthUnits);
  humidityUnits: string[] = Object.values(humidityUnits);
  voltageUnits: string[] = Object.values(voltageUnits);
  amperageUnits: string[] = Object.values(amperageDrawUnits);
  particleConcentrationUnits: string[] = Object.values(particleConcentrationUnits);
  timeUnits: string[] = Object.values(timeUnits);

  constructor() {
  }

  convertUnitOnRecentData(measurement: Measurement, targetUnit: string): Measurement {
    let convertedValue;
    if (measurement.value == "-") {
      measurement.unit = targetUnit;
      return measurement;
    }
    if (this.pressureUnits.includes(measurement.unit)) {
      convertedValue = this.changeUnitPressure(measurement.value, measurement.unit, targetUnit);
    } else if (this.temperatureUnits.includes(measurement.unit)) {
      convertedValue = this.changeUnitTemperature(measurement.value, measurement.unit, targetUnit);
    } else if (this.pmUnits.includes(measurement.unit)) {
      convertedValue = this.changeUnitPm(measurement.value, measurement.unit, targetUnit);
    } else if (this.powerUnits.includes(measurement.unit)) {
      convertedValue = this.changeUnitPower(measurement.value, measurement.unit, targetUnit);
    } else if (this.lengthUnits.includes(measurement.unit)) {
      convertedValue = this.changeUnitLength(measurement.value, measurement.unit, targetUnit);
    } else if (this.humidityUnits.includes(measurement.unit)) {
      convertedValue = this.changeUnitHumidity(measurement.value, measurement.unit, targetUnit);
    } else if (this.voltageUnits.includes(measurement.unit)) {
      convertedValue = this.changeUnitVoltage(measurement.value, measurement.unit, targetUnit);
    } else if (this.amperageUnits.includes(measurement.unit)) {
      convertedValue = this.changeUnitAmperage(measurement.value, measurement.unit, targetUnit);
    } else if (this.particleConcentrationUnits.includes(measurement.unit)) {
      convertedValue = this.changeUnitConcentration(measurement.value, measurement.unit, targetUnit);
    } else if (this.timeUnits.includes(measurement.unit)) {
      convertedValue = this.changeUnitTime(measurement.value, measurement.unit, targetUnit);
    } else {
      convertedValue = measurement.value;
      targetUnit = measurement.unit;
    }
    measurement.value = convertedValue;
    measurement.unit = targetUnit;
    return measurement;
  }

  convertUnitOnChartData(measurement: MeasurementDTO, previousUnit: string, targetUnit: string): MeasurementDTO {
    let convertedValue = measurement.value;
    if (this.pressureUnits.includes(previousUnit)) {
      convertedValue = this.changeUnitPressure(measurement.value, previousUnit, targetUnit);
    } else if (this.temperatureUnits.includes(previousUnit)) {
      convertedValue = this.changeUnitTemperature(measurement.value, previousUnit, targetUnit);
    } else if (this.pmUnits.includes(previousUnit)) {
      convertedValue = this.changeUnitPm(measurement.value, previousUnit, targetUnit);
    } else if (this.timeUnits.includes(previousUnit)) {
      convertedValue = this.changeUnitTime(measurement.value, previousUnit, targetUnit);
    } else if (this.particleConcentrationUnits.includes(previousUnit)) {
      convertedValue = this.changeUnitConcentration(measurement.value, previousUnit, targetUnit);
    } else if (this.amperageUnits.includes(previousUnit)) {
      convertedValue = this.changeUnitAmperage(measurement.value, previousUnit, targetUnit);
    } else if (this.voltageUnits.includes(previousUnit)) {
      convertedValue = this.changeUnitVoltage(measurement.value, previousUnit, targetUnit);
    } else if (this.humidityUnits.includes(previousUnit)) {
      convertedValue = this.changeUnitHumidity(measurement.value, previousUnit, targetUnit);
    } else if (this.lengthUnits.includes(previousUnit)) {
      convertedValue = this.changeUnitLength(measurement.value, previousUnit, targetUnit);
    } else if (this.powerUnits.includes(previousUnit)) {
      convertedValue = this.changeUnitPower(measurement.value, previousUnit, targetUnit);
    }
    measurement.value = convertedValue;
    return measurement;
  }

  private changeUnitTemperature(currentValue: any, previousUnit: string, targetUnit: string): number {
    let newVal = 0;
    switch (previousUnit) {
      case temperatureUnits.C:
        switch (targetUnit) {
          case temperatureUnits.K:
            newVal = currentValue + 273.15;
            break;
          case temperatureUnits.C:
            newVal = currentValue;
            break;
          case temperatureUnits.F:
            newVal = currentValue * 1.8 + 32;
            break
        }
        break;
      case temperatureUnits.F:
        switch (targetUnit) {
          case temperatureUnits.K:
            newVal = (5 * (currentValue + 459.67)) / 9;
            break;
          case temperatureUnits.C:
            newVal = (5 * (currentValue - 32)) / 9;
            break;
          case temperatureUnits.F:
            newVal = currentValue;
            break
        }
        break;
      case temperatureUnits.K:
        switch (targetUnit) {
          case temperatureUnits.K:
            newVal = currentValue;
            break;
          case temperatureUnits.C:
            newVal = currentValue - 273.15;
            break;
          case temperatureUnits.F:
            newVal = 1.8 * (currentValue - 273.15) + 32;
            break
        }
        break;

    }
    return Number(Number(newVal).toFixed(2));
  }

  private changeUnitPressure(currentValue: any, previousUnit: string, targetUnit: string): number {
    let convertedValue;
    switch (previousUnit) {
      case pressureUnits.PASCALS:
        switch (targetUnit) {
          case pressureUnits.HECTO_PASCALS:
            convertedValue = 0.1 * currentValue;
            break;
          case pressureUnits.TECHNICAL_ATMOSPHERE:
            convertedValue = 0.000010197 * currentValue;
            break;
          case pressureUnits.PASCALS:
            convertedValue = currentValue;
            break;
          case pressureUnits.BAR:
            convertedValue = currentValue * 0.00001;
        }
        break
      case pressureUnits.HECTO_PASCALS:
        switch (targetUnit) {
          case pressureUnits.PASCALS:
            convertedValue = 100 * currentValue;
            break
          case pressureUnits.TECHNICAL_ATMOSPHERE:
            convertedValue = 0.0010197 * currentValue;
            break
          case pressureUnits.HECTO_PASCALS:
            convertedValue = currentValue;
            break;
          case pressureUnits.BAR:
            convertedValue = currentValue * 0.001;
        }
        break
      case pressureUnits.TECHNICAL_ATMOSPHERE:
        switch (targetUnit) {
          case pressureUnits.PASCALS:
            convertedValue = 98066.5 * currentValue;
            break
          case pressureUnits.HECTO_PASCALS:
            convertedValue = 980.665 * currentValue;
            break
          case pressureUnits.TECHNICAL_ATMOSPHERE:
            convertedValue = currentValue;
            break;
          case pressureUnits.BAR:
            convertedValue = currentValue * 0.980665;
        }
        break;
      case pressureUnits.BAR:
        switch (targetUnit) {
          case pressureUnits.PASCALS:
            convertedValue = 100000 * currentValue;
            break;
          case pressureUnits.HECTO_PASCALS:
            convertedValue = 1000 * currentValue;
            break;
          case pressureUnits.TECHNICAL_ATMOSPHERE:
            convertedValue = 1.0197 * currentValue;
            break;
          case pressureUnits.BAR:
            convertedValue = currentValue;
        }
    }
    return Number(Number(convertedValue).toFixed(2));
  }

  private changeUnitPm(currentValue: any, previousUnit: string, targetUnit: string): number {
    let convertedValue;
    switch (previousUnit) {
      case pmUnits.MICROGRAM_PER_METER:
        switch (targetUnit) {
          case pmUnits.GRAM_PER_METER:
            convertedValue = 0.000001 * currentValue;
            break
          case pmUnits.MICROGRAM_PER_METER:
            convertedValue = currentValue;
        }
        break
      case pmUnits.GRAM_PER_METER:
        switch (targetUnit) {
          case pmUnits.MICROGRAM_PER_METER:
            convertedValue = 1000000 * currentValue;
            break
          case pmUnits.GRAM_PER_METER:
            convertedValue = currentValue;
        }
    }
    return Number(Number(convertedValue).toFixed(2));
  }

  private changeUnitPower(currentValue: any, previousUnit: string, targetUnit: string): number {
    let convertedValue;
    switch (previousUnit) {
      case powerUnits.WATT:
        switch (targetUnit) {
          case powerUnits.MEGA_WATT:
            convertedValue = 0.000001 * currentValue;
            break
          case powerUnits.WATT:
            convertedValue = currentValue;
        }
        break
      case powerUnits.MEGA_WATT:
        switch (targetUnit) {
          case powerUnits.WATT:
            convertedValue = 1000000 * currentValue;
            break
          case powerUnits.MEGA_WATT:
            convertedValue = currentValue;
        }
    }
    return Number(Number(convertedValue).toFixed(2));
  }

  private changeUnitLength(currentValue: any, previousUnit: string, targetUnit: string): number {
    let convertedValue;
    switch (previousUnit) {
      case lengthUnits.KM:
        switch (targetUnit) {
          case lengthUnits.M:
            convertedValue = 1000 * currentValue;
            break
          case lengthUnits.CM:
            convertedValue = 100000 * currentValue;
            break
          case lengthUnits.KM:
            convertedValue = currentValue;
        }
        break
      case lengthUnits.M:
        switch (targetUnit) {
          case lengthUnits.M:
            convertedValue = currentValue;
            break
          case lengthUnits.KM:
            convertedValue = 0.001 * currentValue;
            break
          case lengthUnits.CM:
            convertedValue = 100 * currentValue;
        }
        break
      case lengthUnits.CM:
        switch (targetUnit) {
          case lengthUnits.KM:
            convertedValue = 0.00001 * currentValue;
            break
          case lengthUnits.M:
            convertedValue = 0.01 * currentValue;
            break
          case lengthUnits.CM:
            convertedValue = currentValue;
        }
    }
    return Number(Number(convertedValue).toFixed(2));
  }

  private changeUnitHumidity(currentValue: any, previousUnit: string, targetUnit: string): number {
    let convertedValue;
    switch (previousUnit) {
      case humidityUnits.KG_KG:
        switch (targetUnit) {
          case humidityUnits.G_KG:
            convertedValue = 1000 * currentValue;
            break
          case humidityUnits.KG_KG:
            convertedValue = currentValue;
        }
        break
      case humidityUnits.G_KG:
        switch (targetUnit) {
          case humidityUnits.KG_KG:
            convertedValue = 0.001 * currentValue;
            break
          case humidityUnits.G_KG:
            convertedValue = currentValue;
        }
    }
    return Number(Number(convertedValue).toFixed(2));
  }

  private changeUnitVoltage(currentValue: any, previousUnit: string, targetUnit: string): number {
    let convertedValue;
    switch (previousUnit) {
      case voltageUnits.VOLT:
        switch (targetUnit) {
          case voltageUnits.ABVOLT:
            convertedValue = 100000000 * currentValue;
            break
          case voltageUnits.STATVOLT:
            convertedValue = 0.0033356405 * currentValue;
            break
          case voltageUnits.VOLT:
            convertedValue = currentValue;
        }
        break
      case voltageUnits.ABVOLT:
        switch (targetUnit) {
          case voltageUnits.ABVOLT:
            convertedValue = currentValue;
            break
          case voltageUnits.STATVOLT:
            convertedValue = 0.00000000003335641 * currentValue;
            break
          case voltageUnits.VOLT:
            convertedValue = 0.00000001 * currentValue;
        }
        break
      case voltageUnits.STATVOLT:
        switch (targetUnit) {
          case voltageUnits.ABVOLT:
            convertedValue = 29979245800 * currentValue;
            break
          case voltageUnits.STATVOLT:
            convertedValue = currentValue;
            break
          case voltageUnits.VOLT:
            convertedValue = 299.792458 * currentValue;
        }
    }
    return Number(Number(convertedValue).toFixed(2));
  }

  private changeUnitAmperage(currentValue: any, previousUnit: string, targetUnit: string): number {
    let convertedValue;
    switch (previousUnit) {
      case amperageDrawUnits.MILI_AMPER:
        switch (targetUnit) {
          case amperageDrawUnits.MICRO_AMPER:
            convertedValue = 1000 * currentValue;
            break
          case amperageDrawUnits.AMPER:
            convertedValue = 0.001 * currentValue;
            break
          case amperageDrawUnits.MILI_AMPER:
            convertedValue = currentValue
        }
        break
      case amperageDrawUnits.MICRO_AMPER:
        switch (targetUnit) {
          case amperageDrawUnits.MICRO_AMPER:
            convertedValue = currentValue;
            break
          case amperageDrawUnits.AMPER:
            convertedValue = 0.000001 * currentValue;
            break
          case amperageDrawUnits.MILI_AMPER:
            convertedValue = 0.001 * currentValue;
        }
        break
      case amperageDrawUnits.AMPER:
        switch (targetUnit) {
          case amperageDrawUnits.MICRO_AMPER:
            convertedValue = 1000000 * currentValue;
            break
          case amperageDrawUnits.AMPER:
            convertedValue = currentValue;
            break
          case amperageDrawUnits.MILI_AMPER:
            convertedValue = 1000 * currentValue;
        }
    }
    return Number(Number(convertedValue).toFixed(2));
  }

  private changeUnitConcentration(currentValue: any, previousUnit: string, targetUnit: string): number {
    let convertedValue;
    switch (previousUnit) {
      case particleConcentrationUnits.PART_M:
        switch (targetUnit) {
          case particleConcentrationUnits.PART_M:
            convertedValue = currentValue;
            break
          case particleConcentrationUnits.PART_CM:
            convertedValue = 0.000001 * currentValue;
        }
        break
      case particleConcentrationUnits.PART_CM:
        switch (targetUnit) {
          case particleConcentrationUnits.PART_M:
            convertedValue = 1000000 * currentValue;
            break
          case particleConcentrationUnits.PART_CM:
            convertedValue = currentValue;
        }
    }
    return Number(Number(convertedValue).toFixed(2));
  }

  private changeUnitTime(currentValue: any, previousUnit: string, targetUnit: string): number {
    let convertedValue;
    switch (previousUnit) {
      case timeUnits.DAYS:
        switch (targetUnit) {
          case timeUnits.DAYS:
            convertedValue = currentValue;
            break
          case timeUnits.SECONDS:
            convertedValue = 24 * 3600 * currentValue;
            break
          case timeUnits.HOURS:
            convertedValue = 24 * currentValue;
        }
        break
      case timeUnits.HOURS:
        switch (targetUnit) {
          case timeUnits.DAYS:
            convertedValue = 0.04167 * currentValue;
            break
          case timeUnits.SECONDS:
            convertedValue = 3600 * currentValue;
            break
          case timeUnits.HOURS:
            convertedValue = currentValue;
        }
        break
      case timeUnits.SECONDS:
        switch (targetUnit) {
          case timeUnits.DAYS:
            convertedValue = 0.00001157407 * currentValue;
            break
          case timeUnits.SECONDS:
            convertedValue = currentValue;
            break
          case timeUnits.HOURS:
            convertedValue = 0.00027777 * currentValue;
        }
    }
    return Number(Number(convertedValue).toFixed(2));
  }
}
