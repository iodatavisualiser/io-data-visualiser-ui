import {Component, Inject, OnInit} from "@angular/core";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {EndpointDTO} from "../../shared/dtos/endpoint.dto";
import {FormControl, FormGroup} from "@angular/forms";
import {ChartDataRepository} from "../../charts-page/services/chart-data.repository";
import {AvailableChartsService} from "../../charts-page/services/available-charts.service";
import {Router} from "@angular/router";
import {AvailableEndpointsService} from "../../home-page/sevices/available-endpoints.service";
import {ConfigDataDTO} from "../../home-page/dtos/config-data.dto";
import {ChartDataDTO} from "../../charts-page/dtos/chart-data.dto";
import {MultipleStationsDataDTO} from "../../charts-page/dtos/multiple-stations-data.dto";
import {ChartCounterService} from "../../shared/services/chart-counter.service";

@Component({
  selector: 'app-data-selection-dialog',
  templateUrl: './data-selection-dialog.component.html',
  styleUrls: ['./data-selection-dialog.component.scss']
})
export class DataSelectionDialogComponent implements OnInit {

  endpoints: EndpointDTO[];
  maxMonthRange: number;
  minDate: Date;
  maxDate: Date;
  dataControl = new FormControl();
  range = new FormGroup({
    start: new FormControl(),
    end: new FormControl()
  });
  configData: ConfigDataDTO;

  availableDataList: string[];

  constructor(private readonly dialogRef: MatDialogRef<DataSelectionDialogComponent>,
              private readonly chartDataRepository: ChartDataRepository,
              private readonly availableChartsService: AvailableChartsService,
              private readonly availableEndpoinstService: AvailableEndpointsService,
              private readonly router: Router,
              private readonly chartCounterService: ChartCounterService,
              @Inject(MAT_DIALOG_DATA) data: { endpoints: EndpointDTO[], maxMonthRange: number }) {
    this.endpoints = data.endpoints;
    this.maxMonthRange = data.maxMonthRange;
  }

  ngOnInit() {
    this.availableEndpoinstService.configData$.subscribe(configData => this.configData = configData);
    this.maxDate = new Date();
    this.minDate = new Date(new Date().setMonth(this.maxDate.getMonth() - this.maxMonthRange));
    this.availableDataList = this.endpoints[0]?.measurements.map((item: any) => item.name);
  }

  onSubmit() {
    const startDate = new Date(this.range.controls['start'].value);
    const endDate = new Date(this.range.controls['end'].value);
    const values = this.dataControl.value;
    values.forEach((value: string) => {
      const targetUnit = this.endpoints[0].measurements.filter(measurement => measurement.name === value)[0].unit;
      this.chartCounterService.incrementCounter();
      this.chartDataRepository.getMultipleStationsData(this.endpoints, startDate, endDate, value, this.configData, targetUnit)
        .subscribe(multipleStationsData => {
          const title = this.endpoints.length > 1 ? `Stations comparision \\ ${value}` : `${this.endpoints[0].id} \\ ${value}`;
          const multipleStations = {
            from: startDate,
            to: endDate,
            type: value,
            unit: targetUnit,
            stations: multipleStationsData
          } as MultipleStationsDataDTO;
          const chartData = {
            title,
            multipleStationsData: multipleStations
          } as ChartDataDTO;
          this.availableChartsService.addChartData(chartData);
        });
    });
    this.dialogRef.close();
    this.router.navigate(['my-charts']);
  }
}
