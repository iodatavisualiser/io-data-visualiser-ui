import {Component} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {SendEmailDialogComponent} from "../send-email/send-email-dialog.component";

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent {
  constructor(public readonly dialog: MatDialog) {
  }

  openDialog() {
    const dialogRef = this.dialog.open(SendEmailDialogComponent, {
      disableClose: true
    });
  }
}
