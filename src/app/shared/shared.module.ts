import {NgModule} from '@angular/core';
import {RouterModule} from "@angular/router";
import {TopPanelComponent} from "./top-panel/top-panel.component";
import {MatButtonModule} from "@angular/material/button";
import {MatIconModule} from "@angular/material/icon";
import {CommonModule} from "@angular/common";
import {FooterComponent} from "./footer/footer.component";
import {AvailableEndpointsService} from "../home-page/sevices/available-endpoints.service";
import {MatDialogModule} from "@angular/material/dialog";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {SendEmailDialogComponent} from "./send-email/send-email-dialog.component";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";
import {SendMailService} from "./services/send-mail.service";
import {SendEmailErrorStateMatcherService} from "./services/send-email-error-state-matcher.service";
import {ReactiveFormsModule} from "@angular/forms";
import {HttpClientModule} from "@angular/common/http";
import {MatProgressSpinnerModule} from "@angular/material/progress-spinner";
import {MatTooltipModule} from "@angular/material/tooltip";
import {ChartCounterService} from "./services/chart-counter.service";
import {DataNameParserService} from "./services/data-name-parser.service";
import {AboutUsComponent} from "./modal/about-us.component";

@NgModule({
  declarations: [TopPanelComponent, FooterComponent, SendEmailDialogComponent, AboutUsComponent],
  imports: [
    RouterModule,
    MatButtonModule,
    MatIconModule,
    CommonModule,
    MatTooltipModule,
    MatDialogModule,
    BrowserAnimationsModule,
    MatFormFieldModule,
    MatInputModule,
    ReactiveFormsModule,
    HttpClientModule,
    MatProgressSpinnerModule
  ],
  providers: [
    AvailableEndpointsService,
    SendMailService,
    SendEmailErrorStateMatcherService,
    ChartCounterService,
    DataNameParserService
  ],
  exports: [TopPanelComponent, FooterComponent, SendEmailDialogComponent, AboutUsComponent]
})
export class SharedModule {
}
