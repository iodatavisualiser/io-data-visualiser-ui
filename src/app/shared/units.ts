export interface DataType {
  name: string;
  units: string[];
  currentUnit: string;
  event: any;
  show: boolean;
}

export enum temperatureUnits {
  K = "K",
  C = "°C",
  F = "°F"
}

export enum pressureUnits {
  HECTO_PASCALS = "hPa",
  PASCALS = "Pa",
  TECHNICAL_ATMOSPHERE = "atm",
  BAR = "Ba"
}

export enum pmUnits {
  MICROGRAM_PER_METER = "µg/m3",
  GRAM_PER_METER = "g/m3"
}

export enum humidityUnits {
  G_KG = "g/kg",
  KG_KG = "kg/kg"
}

export enum powerUnits {
  WATT = "Wat",
  MEGA_WATT = "MegaWat",
}

export enum lengthUnits {
  CM = "cm",
  KM = "km",
  M = "m"
}

export enum voltageUnits {
  VOLT = "V",
  ABVOLT = "abV",
  STATVOLT = "statV"
}

export enum amperageDrawUnits {
  MILI_AMPER = "mA",
  MICRO_AMPER = "μA",
  AMPER = "A"
}

export enum particleConcentrationUnits {
  PART_CM = "particles/cm3",
  PART_M = "particles/m3"
}

export enum timeUnits {
  DAYS = "days",
  HOURS = "hours",
  SECONDS = "seconds"
}

export class Measurement {
  name: string;
  value: number | string | boolean;
  unit: string;

  constructor(name: string, value: (number | string | boolean), unit: string) {
    this.name = name;
    this.value = value;
    this.unit = unit;
  }

  getValueOfMeasurement(search: string): (number | string | boolean) {

    if (this.name === search) {
      return this.value;
    }
    return null;
  }
}
