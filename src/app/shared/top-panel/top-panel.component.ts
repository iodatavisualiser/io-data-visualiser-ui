import {ChangeDetectionStrategy, Component, Input} from '@angular/core';
import {PageType} from "../enum/page-type.enum";
import {MatDialog} from "@angular/material/dialog";
import {AboutUsComponent} from "../modal/about-us.component";

@Component({
  selector: 'app-top-panel',
  templateUrl: './top-panel.component.html',
  styleUrls: ['./top-panel.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TopPanelComponent {

  @Input()
  pageActive: PageType;

  pageOptions = PageType;

  readonly PAGE_TITLE = "Data Visualiser for DataHub";
  readonly HOME = "Home";
  readonly MY_DATASETS = "My datasets";
  readonly MY_CHARTS = "My charts";
  readonly ABOUT_US = "About us";

  constructor(private readonly dialog: MatDialog) {
  }

  openAboutUsModal() {
    const dialogRef = this.dialog.open(AboutUsComponent);
  }
}
