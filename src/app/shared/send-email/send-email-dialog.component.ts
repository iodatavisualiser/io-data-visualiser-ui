import {Component} from "@angular/core";
import {FormControl, Validators} from "@angular/forms";
import {MatDialogRef} from "@angular/material/dialog";
import {Router} from "@angular/router";
import {SendEmailErrorStateMatcherService} from "../services/send-email-error-state-matcher.service";
import {SendMailService} from "../services/send-mail.service";
import {catchError, finalize, throwError} from "rxjs";
import {error} from "highcharts";

@Component({
  selector: 'app-send-email-dialog',
  templateUrl: './send-email-dialog.component.html',
  styleUrls: ['./send-email-dialog.component.scss']
})
export class SendEmailDialogComponent {

  constructor(private readonly dialogRef: MatDialogRef<SendEmailDialogComponent>,
              private readonly router: Router,
              private readonly sendMailService: SendMailService) {
  }

  emailFormControl = new FormControl('', [Validators.required, Validators.email]);
  messageFormControl = new FormControl('', [Validators.required, Validators.minLength(10)]);
  matcher = new SendEmailErrorStateMatcherService();
  loading: boolean = false;
  mailSent: boolean = false;
  error: boolean = false;

  onSubmit(): void {
    if (!this.emailFormControl.invalid && !this.messageFormControl.invalid) {
      const email = this.emailFormControl.value;
      const message = this.messageFormControl.value;
      this.loading = true;
      this.sendMailService.sendEmail(email, message).pipe(
        catchError(err => {
          this.error = true;
          return throwError(error);
        }),
        finalize(() => {
          this.loading = false;
        })
      ).subscribe(() => this.mailSent = true);
    }
  }

  tryAgain() {
    this.error = false;
  }
}
