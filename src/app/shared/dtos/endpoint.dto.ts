import {Measurement} from "../units";

export interface EndpointDTO {
  id: string;
  measurements: Measurement[];
}
