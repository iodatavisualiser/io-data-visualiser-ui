import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DataNameParserService {

  constructor() {
  }

  public parseName(dataType: string) {
    var re = /_/gi;
    return dataType.split("/").slice(-1)[0].replace(re, ".")
  }
}
