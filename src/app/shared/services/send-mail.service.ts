import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";

@Injectable()
export class SendMailService {

  constructor(private readonly http: HttpClient) {
  }

  private readonly endpointName = "https://formspree.io/f/mgedrzok"

  // mail: iovisualiser@gmail.com

  public sendEmail(email: string, message: string): Observable<any> {
    const formData = new FormData();
    formData.append("email", email);
    formData.append("message", message);
    return this.http.post(this.endpointName, formData);
  }
}
