import {Injectable} from "@angular/core";

@Injectable()
export class ChartCounterService {
  public chartCounter = 0;

  incrementCounter() {
    this.chartCounter += 1;
  }

  getCounter(): number {
    return this.chartCounter;
  }
}
