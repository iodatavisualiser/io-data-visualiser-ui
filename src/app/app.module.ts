import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {AppComponent} from './app.component';
import {RouterModule} from '@angular/router';
import {HomePageModule} from './home-page/home-page.module'
import {SharedModule} from './shared/shared.module';
import {ChartsPageModule} from "./charts-page/charts-page.module";
import {RecentDatasetsComponent} from './recent-datasets/recent-datasets.component';
import {RecentDatasetsModule} from './recent-datasets/recent-datasets.module';
import {CommonModule} from "@angular/common";
import {HomePageComponent} from "./home-page/home-page.component";
import {ChartsPageComponent} from "./charts-page/charts-page-component";
import {ChartCounterService} from "./shared/services/chart-counter.service";
import {MatGridListModule} from "@angular/material/grid-list";
import {MatDatepickerModule} from "@angular/material/datepicker";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";
import {MatCardModule} from "@angular/material/card";
import {MatNativeDateModule, MatOptionModule} from "@angular/material/core";
import {MatSelectModule} from "@angular/material/select";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MatSlideToggleModule} from "@angular/material/slide-toggle";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {MatRadioModule} from "@angular/material/radio";
import {MatChipsModule} from "@angular/material/chips";
import {MatIconModule} from "@angular/material/icon";
import {MatTableModule} from "@angular/material/table";
import {MatPaginatorModule} from "@angular/material/paginator";
import {EndpointViewModule} from "./endpoint-view/endpoint-view.module";
import {EndpointViewComponent} from "./endpoint-view/endpoint-view.component";

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot([
      {path: 'home', component: HomePageComponent},
      {path: 'my-datasets', component: RecentDatasetsComponent},
      {path: 'my-charts', component: ChartsPageComponent},
      {path: 'single-station/:id', component: EndpointViewComponent},
      {path: '**', redirectTo: '/home', pathMatch: 'full'}
    ]),
    SharedModule,
    HomePageModule,
    ChartsPageModule,
    RecentDatasetsModule,
    CommonModule,
    MatGridListModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatInputModule,
    MatCardModule,
    MatOptionModule,
    MatSelectModule,
    ReactiveFormsModule,
    MatSlideToggleModule,
    FormsModule,
    MatFormFieldModule,
    MatNativeDateModule,
    ReactiveFormsModule,
    BrowserModule,
    BrowserAnimationsModule,
    MatRadioModule,
    MatChipsModule,
    MatIconModule,
    MatTableModule,
    MatPaginatorModule,
    EndpointViewModule
  ],
  providers: [ChartCounterService],
  exports: [AppComponent],
  bootstrap: [AppComponent]
})
export class AppModule {
}
