import {Component, OnInit} from '@angular/core';
import {DataType, Measurement} from "../shared/units";
import {
  DateRange,
  DefaultMatCalendarRangeStrategy,
  MAT_DATE_RANGE_SELECTION_STRATEGY
} from "@angular/material/datepicker";
import {MatSlideToggleChange} from "@angular/material/slide-toggle";
import {MatChip} from '@angular/material/chips';
import {SingleStationDataDTO} from "../charts-page/dtos/single-station-data.dto.";
import {MultipleStationsDataDTO} from "../charts-page/dtos/multiple-stations-data.dto";
import {AvailableEndpointsService} from "../home-page/sevices/available-endpoints.service";
import {catchError, combineLatest, finalize, forkJoin, throwError} from "rxjs";
import {ActivatedRoute} from "@angular/router";
import {HttpClient, HttpErrorResponse, HttpStatusCode} from "@angular/common/http";
import {ConfigDataDTO} from "../home-page/dtos/config-data.dto";
import {ChartDataDTO} from "../charts-page/dtos/chart-data.dto";
import {ChartDataRepository} from "../charts-page/services/chart-data.repository";
import {EndpointDTO} from "../shared/dtos/endpoint.dto";
import {DataConverterService} from "../recent-datasets/services/data-converter.service";
import {DataNameParserService} from "../shared/services/data-name-parser.service";

@Component({
  selector: 'app-endpoint-view',
  templateUrl: './endpoint-view.component.html',
  styleUrls: ['./endpoint-view.component.css'],
  providers: [
    {
      provide: MAT_DATE_RANGE_SELECTION_STRATEGY,
      useClass: DefaultMatCalendarRangeStrategy,
    },
  ],
})

export class EndpointViewComponent implements OnInit {

  //data
  stationId: string;
  stationUrl: string;
  error: HttpErrorResponse;
  configData: ConfigDataDTO;
  errorMessage: string;
  loading: boolean = true;
  dataTypes: DataType[] = [];
  recentMeasurements: Measurement[] = [];
  latestActivity: Date;
  recentDataEndpoint = "http://localhost:8080/recent";
  chartData: ChartDataDTO[] = [];
  endpoint: EndpointDTO;
  selectedDateRange: DateRange<Date>;
  readonly reload = "Reload";

  //responsive
  showCharts: boolean = true;
  breakpoint: number;
  filterRowspan: number;
  filterRowHeight: string;
  calendarRowspan: number = 1;
  recentCols: number;
  recentRowHeight: string;
  mainListRowHeight: string;
  detailedDataCols: number;
  detailedDataStyle: any;
  detailedDataRowHeight: string;
  singleDetailedDataRowHeight: string;
  unitStyle: any
  dataRowspan: number;

  constructor(private readonly availableEndpointsService: AvailableEndpointsService,
              private readonly activatedRoute: ActivatedRoute,
              private readonly http: HttpClient,
              private readonly chartDataRepository: ChartDataRepository,
              private readonly dataConverterService: DataConverterService,
              readonly nameParser: DataNameParserService) {
  }

  onChange(value: MatSlideToggleChange) {
    this.showCharts = value.checked;
  }

  onTimeRangeChange(date: Date): void {
    if (
      this.selectedDateRange &&
      this.selectedDateRange.start &&
      date > this.selectedDateRange.start &&
      !this.selectedDateRange.end
    ) {
      this.selectedDateRange = new DateRange(
        this.selectedDateRange.start,
        date
      );
      this.loading = true;
      this.prepareChartData();
    } else {
      this.selectedDateRange = new DateRange(date, null);
    }
  }

  toggleSelection(chip: MatChip, dataType: DataType) {
    chip.toggleSelected();
    dataType.show = !dataType.show
  }

  ngOnInit() {
    combineLatest(
      [
        this.activatedRoute.paramMap,
        this.availableEndpointsService.configData$
      ])
      .pipe(
        catchError(err => {
            this.error = err;
            this.setErrorMessage(this.error.status);
            return throwError(err);
          }
        )
      )
      .subscribe(([paramMap, configData]) => {
          this.stationId = paramMap.get('id');
          this.configData = configData;
          const stations = this.configData?.stations.map(station => station.name);
          if (!this.stationId || !this.configData || !stations.includes(this.stationId)) {
            this.error = new HttpErrorResponse({status: HttpStatusCode.NoContent});
            this.setErrorMessage(this.error.status);
            this.loading = false;
            throw this.error;
          } else {
            this.stationUrl = this.configData.stations.filter(station => station.name === this.stationId)[0].endpoint;
            const dateNow = new Date();
            const weekAgo = new Date(dateNow.getFullYear(), dateNow.getMonth(), dateNow.getDate() - 7);
            this.selectedDateRange = new DateRange<Date>(weekAgo, dateNow);
            this.prepareDataTypes();
            this.prepareRecentData();
          }
        }
      );
  }

  onResize(event: UIEvent) {
    const window = event.target as Window
    var additionalDatatypes = this.filterDataTypes().length - 7 > 0 ? this.filterDataTypes().length - 7 : 0
    this.recentCols = (window.innerWidth >= 570) ? 3 : 1;
    this.recentRowHeight = (window.innerWidth >= 1400) ? "4:1" : (window.innerWidth >= 1050) ? "3:1" : (window.innerWidth >= 760) ? "2:1" : (window.innerWidth >= 570) ? "1.2:1" : "2:1";
    this.breakpoint = (window.innerWidth <= 900) ? 1 : 3;
    this.unitStyle = (window.innerWidth >= 900) ? {} : (window.innerWidth >= 500) ? {'top.px': 250} : (window.innerWidth >= 390) ? {'top.px': 450} : {'top.px': 550};
    this.unitStyle = (window.innerWidth < 380 && this.dataTypes.length > 4) ? {'top.px': 250} : {};

    this.filterRowspan = (window.innerWidth <= 900) ? 3 : 1;
    this.calendarRowspan = (window.innerWidth >= 550) ? 1 : 2;
    this.dataRowspan = (window.innerWidth <= 500) ? 2 : 1;
    this.filterRowHeight = (window.innerWidth >= 550) ? "1:1" : (window.innerWidth >= 400) ? "2.1:2.3" : "2:1.5";
    this.mainListRowHeight = (window.innerWidth >= 2000) ? "1.1:1" : (window.innerWidth >= 1100) ? "1.2:1" : (window.innerWidth >= 900) ? "1.3:1" : "1:2";
    this.detailedDataCols = (window.innerWidth <= 1500) ? 1 : 2;
    this.detailedDataStyle = (window.innerWidth <= 600) ? "top:" + (90 * (additionalDatatypes) + 2000).toString() + "px" : (window.innerWidth <= 900) ? "top:" + (110 * (additionalDatatypes) + 2000).toString() + "px" : (window.innerWidth >= 2100) ? "top:" + (100 * (additionalDatatypes) + 1000).toString() : "top:" + (60 * additionalDatatypes + 900).toString() + "px"
    this.detailedDataRowHeight = (window.innerWidth > 2000) ? "1.5:1" : (window.innerWidth > 1400) ? "1:1" : (window.innerWidth > 1000) ? "1:1.5" : (window.innerWidth > 700) ? "1:2" : (window.innerWidth > 500) ? "1:2.6" : (window.innerWidth > 450) ? "1:3" : "1:4";
    this.singleDetailedDataRowHeight = (window.innerWidth > 2000) ? "3:1" : (window.innerWidth > 1400) ? "2:1" : (window.innerWidth > 1000) ? "1.5:1" : (window.innerWidth > 700) ? "1:1" : (window.innerWidth > 500) ? "1:1.3" : (window.innerWidth > 450) ? "1:1.5" : "1:2";

  }

  private setSize() {
    var additionalDatatypes = this.filterDataTypes().length - 7 > 0 ? this.filterDataTypes().length - 7 : 0
    this.recentCols = (window.innerWidth >= 570) ? 3 : 1;
    this.recentRowHeight = (window.innerWidth >= 1400) ? "4:1" : (window.innerWidth >= 1050) ? "3:1" : (window.innerWidth >= 760) ? "2:1" : (window.innerWidth >= 570) ? "1.2:1" : "2:1";
    this.breakpoint = (window.innerWidth <= 900) ? 1 : 3;
    this.unitStyle = (window.innerWidth >= 900) ? {} : (window.innerWidth >= 500) ? {'top.px': 250} : (window.innerWidth >= 390) ? {'top.px': 450} : {'top.px': 550};
    this.unitStyle = (window.innerWidth < 380 && this.dataTypes.length > 4) ? {'top.px': 250} : {};

    this.filterRowspan = (window.innerWidth <= 900) ? 3 : 1
    this.calendarRowspan = (window.innerWidth >= 550) ? 1 : 2;
    this.dataRowspan = (window.innerWidth <= 500) ? 2 : 1;
    this.filterRowHeight = (window.innerWidth >= 550) ? "1:1" : (window.innerWidth >= 400) ? "2.1:2.3" : "2:1.5";
    this.mainListRowHeight = (window.innerWidth >= 2000) ? "1.1:1" : (window.innerWidth >= 1100) ? "1.2:1" : (window.innerWidth >= 900) ? "1.3:1" : "1:2";
    this.detailedDataCols = (window.innerWidth <= 1500) ? 1 : 2;
    this.detailedDataStyle = (window.innerWidth <= 600) ? "top:" + (90 * (additionalDatatypes) + 2000).toString() + "px" : (window.innerWidth <= 900) ? "top:" + (110 * (additionalDatatypes) + 2000).toString() + "px" : (window.innerWidth >= 2100) ? "top:" + (100 * (additionalDatatypes) + 1000).toString() : "top:" + (60 * additionalDatatypes + 900).toString() + "px"
    this.detailedDataRowHeight = (window.innerWidth > 2000) ? "1.5:1" : (window.innerWidth > 1400) ? "1:1" : (window.innerWidth > 1000) ? "1:1.5" : (window.innerWidth > 700) ? "1:2" : (window.innerWidth > 500) ? "1:2.6" : (window.innerWidth > 450) ? "1:3" : "1:4";
    this.singleDetailedDataRowHeight = (window.innerWidth > 2000) ? "3:1" : (window.innerWidth > 1400) ? "2:1" : (window.innerWidth > 1000) ? "1.5:1" : (window.innerWidth > 700) ? "1:1" : (window.innerWidth > 500) ? "1:1.3" : (window.innerWidth > 450) ? "1:1.5" : "1:2";


  }

  private prepareDataTypes() {
    this.configData.sensors.forEach(sensor => {
      const dataType = {
        name: sensor.name,
        units: sensor.possibleUnits,
        currentUnit: sensor.defaultUnit,
        event: sensor.defaultUnit,
        show: (sensor.possibleUnits !== undefined)
      } as DataType;
      this.dataTypes.push(dataType);
    });
  }

  private prepareRecentData(): void {
    this.recentMeasurements = [];
    this.http.get(this.recentDataEndpoint).pipe(
      catchError(err => {
        this.error = err;
        this.setErrorMessage(this.error.status);
        this.loading = false;
        return throwError(err);
      })
    )
      .subscribe((recentData: any) => {
        const stationMeasurements = recentData.data.filter((station: any) => station.name === this.stationUrl)[0].data;
        this.latestActivity = stationMeasurements.map((measurement: any) => new Date(measurement.date)).sort((a: Date, b: Date) => a.getUTCMilliseconds() - b.getUTCMilliseconds())[0];
        stationMeasurements.forEach((measurement: any) => {
          const unit = this.dataTypes.filter(dataType => dataType.name === measurement.name)[0].currentUnit;
          const defaultUnit = this.configData.sensors.find(sensor => sensor.name === measurement.name).defaultUnit;
          if (unit !== defaultUnit) {
            measurement = this.dataConverterService.convertUnitOnChartData(measurement, defaultUnit, unit);
          }
          const newMeasurement = new Measurement(measurement.name, Number(Number(measurement.value).toFixed(2)), unit);
          this.recentMeasurements.push(newMeasurement);
        });
        this.endpoint = {
          id: this.stationId,
          measurements: this.recentMeasurements
        };
        this.prepareChartData();
      });
  }

  private prepareChartData(): void {
    this.chartData = [];
    const requests = this.dataTypes.map(dataType => this.chartDataRepository.getMultipleStationsData([this.endpoint], this.selectedDateRange.start, this.selectedDateRange.end, dataType.name, this.configData, dataType.currentUnit));
    forkJoin(requests)
      .pipe(
        catchError(err => {
          this.error = err;
          this.setErrorMessage(this.error.status);
          return throwError(err);
        }),
        finalize(
          () => {
            this.setSize();
            this.loading = false;
          }
        )
      )
      .subscribe(responses => {
        responses.forEach(stationsData => {
          const title = `${this.stationId} \\ ${stationsData[0].type}`;
          const multipleStations = {
            from: this.selectedDateRange.start,
            to: this.selectedDateRange.end,
            type: stationsData[0].type,
            unit: this.dataTypes.find(dataType => dataType.name === stationsData[0].type).currentUnit,
            stations: stationsData
          } as MultipleStationsDataDTO;
          const chartData = {
            title,
            multipleStationsData: multipleStations
          } as ChartDataDTO;
          this.chartData.push(chartData);
        });
      });
  }

  getMeasurements(key: string): SingleStationDataDTO {
    return this.chartData.find(e => e.multipleStationsData.type === key)?.multipleStationsData.stations[0];
  }

  onSelectedUnitChange(dataType: DataType) {
    this.loading = true;
    this.dataTypes.map(type => {
      if (dataType.name === type.name) {
        type.currentUnit = dataType.event;
        type.event = dataType.event;
      }
    });
    this.prepareRecentData();
  }

  getChartData(key: string): MultipleStationsDataDTO {
    return this.chartData.find(e => e.multipleStationsData.type == key)?.multipleStationsData;
  }

  public getLastMeasurement(key: string): Measurement {
    return this.recentMeasurements.filter(measurement => measurement.name === key)[0];
  }

  private setErrorMessage(status: HttpStatusCode): void {
    if (status === HttpStatusCode.InternalServerError) {
      this.errorMessage = "Sorry, an error has ocurred during data processing. Try again later."
    } else if (status === HttpStatusCode.ServiceUnavailable) {
      this.errorMessage = "Sorry, DataHub server is currently unavailable. Try again later."
    } else if (status === HttpStatusCode.NoContent) {
      this.errorMessage = "Sorry, there is currently no available data based on your configuration file."
    } else {
      this.errorMessage = "Sorry, an error has ocurred. Try again later."
    }
  }

  reloadPage(): void {
    this.ngOnInit();
  }

  filterDataTypes() {
    return this.dataTypes.filter(e => e.units !== undefined && e.currentUnit != "°")
  }
}
