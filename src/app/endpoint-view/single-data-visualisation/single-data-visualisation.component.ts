import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource} from "@angular/material/table";
import {MatPaginator} from "@angular/material/paginator";
import {MeasurementDTO} from "../../charts-page/dtos/measurement.dto";
import {SingleStationDataDTO} from "../../charts-page/dtos/single-station-data.dto.";
import {DateRange} from "@angular/material/datepicker";

@Component({
  selector: 'app-single-data-visualisation',
  templateUrl: './single-data-visualisation.component.html',
  styleUrls: ['./single-data-visualisation.component.css']
})
export class SingleDataVisualisationComponent implements OnInit {
  @Input() measurements: SingleStationDataDTO

  _selectedDateRange: DateRange<Date>
  @Input('selectedDateRange')
  set selectedDateRange(selectedDateRange: DateRange<Date>) {
    this._selectedDateRange = selectedDateRange;
    try {
      this.dataSource = new MatTableDataSource(this.measurements.data.filter(e => e.timestamp <= selectedDateRange.end && e.timestamp >= selectedDateRange.start))
    } catch (e) {

    }
  }

  get selectedDateRange(): DateRange<Date> {
    return this._selectedDateRange;
  }

  displayedColumns: string[] = ['date', 'value']
  dataSource: MatTableDataSource<MeasurementDTO>;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor() {
  }

  ngAfterViewInit() {
    this.dataSource = new MatTableDataSource(this.measurements.data);
    this.dataSource.paginator = this.paginator;
  }

  ngOnInit(): void {
    this.dataSource = new MatTableDataSource(this.measurements.data);
    this.dataSource.paginator = this.paginator;
  }
}
