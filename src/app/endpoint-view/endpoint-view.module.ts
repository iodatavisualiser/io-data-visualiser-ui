import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";
import {MatGridListModule} from "@angular/material/grid-list";
import {MatPaginatorModule} from "@angular/material/paginator";
import {MatTableModule} from "@angular/material/table";
import {EndpointViewComponent} from "./endpoint-view.component";
import {SingleDataVisualisationComponent} from "./single-data-visualisation/single-data-visualisation.component";
import {SharedModule} from "../shared/shared.module";
import {MatCardModule} from "@angular/material/card";
import {MatDatepickerModule} from "@angular/material/datepicker";
import {MatChipsModule} from "@angular/material/chips";
import {MatIconModule} from "@angular/material/icon";
import {MatSlideToggleModule} from "@angular/material/slide-toggle";
import {FormsModule} from "@angular/forms";
import {MatRadioModule} from "@angular/material/radio";
import {ChartsPageModule} from "../charts-page/charts-page.module";
import {AvailableEndpointsService} from "../home-page/sevices/available-endpoints.service";
import {ChartDataRepository} from "../charts-page/services/chart-data.repository";
import {MatProgressSpinnerModule} from "@angular/material/progress-spinner";
import {MatButtonModule} from "@angular/material/button";
import {DataConverterService} from "../recent-datasets/services/data-converter.service";
import {DataNameParserService} from "../shared/services/data-name-parser.service";


@NgModule({
  declarations: [EndpointViewComponent, SingleDataVisualisationComponent],
  imports: [
    MatFormFieldModule,
    CommonModule,
    MatInputModule,
    MatGridListModule,
    MatPaginatorModule,
    MatTableModule,
    CommonModule,
    SharedModule,
    MatCardModule,
    MatDatepickerModule,
    MatChipsModule,
    MatIconModule,
    MatSlideToggleModule,
    FormsModule,
    MatRadioModule,
    ChartsPageModule,
    MatProgressSpinnerModule,
    MatButtonModule
  ],
  exports: [EndpointViewComponent, SingleDataVisualisationComponent],
  providers: [
    AvailableEndpointsService,
    ChartDataRepository,
    DataConverterService,
    DataNameParserService
  ]
})
export class EndpointViewModule {
}
