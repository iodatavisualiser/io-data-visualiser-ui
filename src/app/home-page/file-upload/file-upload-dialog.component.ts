import {Component} from "@angular/core";
import {MatDialogRef} from "@angular/material/dialog";
import {FileUploadService} from "./services/file-upload.service";
import {AvailableEndpointsService} from "../sevices/available-endpoints.service";
import {ConfigDataMapper} from "../sevices/config-data-mapper.service";
import {Router} from "@angular/router";
import {catchError, throwError} from "rxjs";

@Component({
  selector: 'app-file-upload-dialog',
  templateUrl: './file-upload-dialog.component.html',
  styleUrls: ['./file-upload-dialog.component.scss']
})
export class FileUploadDialogComponent {

  readonly requiredFileType = ".json";
  fileToUpload: File;
  fileSelected: boolean;
  error: boolean;


  constructor(private readonly dialogRef: MatDialogRef<FileUploadDialogComponent>,
              private readonly fileUploadService: FileUploadService,
              private readonly availableEndpointsService: AvailableEndpointsService,
              private readonly configDataMapper: ConfigDataMapper,
              private readonly fileReader: FileReader,
              private readonly router: Router) {
  }

  onFileSelected(event: any) {
    this.error = undefined;
    const file = event.target.files[0];

    if (file) {
      this.fileToUpload = file;
      this.fileSelected = true;
    }
  }

  uploadFileToActivity() {
    this.fileReader.addEventListener('error', () => {
      this.error = true;
    });

    this.fileReader.addEventListener('load', e => {
      const configData = this.configDataMapper.mapToConfigDataDTO(<string>e.target.result);
      this.fileUploadService.postConfigData(configData)
        .pipe(
          catchError((err) => {
            this.error = true;
            return throwError(err);
          })
        )
        .subscribe(
          () => {
            this.dialogRef.close();
            this.availableEndpointsService.setConfigData(configData);
            this.goToMyDatasets();
          }
        );
    });
    this.fileReader.readAsText(this.fileToUpload);
  }

  private goToMyDatasets() {
    this.router.navigate(['my-datasets']);
  }
}
