import {Injectable} from "@angular/core";
import {Observable} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {ConfigDataDTO} from "../../dtos/config-data.dto";

@Injectable()
export class FileUploadService {

  private readonly postEndpoint = "http://localhost:8080/config";

  constructor(private readonly httpClient: HttpClient) {
  }

  postConfigData(configData: ConfigDataDTO): Observable<any> {
    return this.httpClient.post(this.postEndpoint, configData);
  }
}
