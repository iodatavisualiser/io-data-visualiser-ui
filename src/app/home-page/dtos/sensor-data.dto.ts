export interface SensorDataDTO {
  readonly name: string;
  readonly defaultUnit: string;
  readonly possibleUnits: string[];
}
