export interface StationDTO {
  readonly name: string;
  readonly endpoint: string
}
