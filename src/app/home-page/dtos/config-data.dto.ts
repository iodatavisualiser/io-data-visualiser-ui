import {SensorDataDTO} from "./sensor-data.dto";
import {StationDTO} from "./station.dto";

export interface ConfigDataDTO {
  readonly stations: StationDTO[];
  readonly maxMonthRange: number;
  readonly sensors: SensorDataDTO[];
}
