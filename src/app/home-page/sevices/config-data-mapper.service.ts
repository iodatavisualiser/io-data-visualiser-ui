import {Injectable} from "@angular/core";
import {ConfigDataDTO} from "../dtos/config-data.dto";
import {SensorDataDTO} from "../dtos/sensor-data.dto";
import {StationDTO} from "../dtos/station.dto";

@Injectable()
export class ConfigDataMapper {

  mapToConfigDataDTO(data: string): ConfigDataDTO {
    const dataObject = JSON.parse(data);
    const stations = dataObject.stations?.map((station: any) => this.mapToStationDTO(station));
    const sensors = dataObject.sensors?.map((sensor: any) => this.mapToSensorDataDTO(sensor));

    return {
      stations,
      maxMonthRange: dataObject.maxMonthRange,
      sensors
    } as ConfigDataDTO;
  }

  mapToStationDTO(station: any): StationDTO {
    return {
      name: station.name,
      endpoint: station.endpoint
    } as StationDTO;
  }

  mapToSensorDataDTO(sensor: any): SensorDataDTO {
    return {
      name: sensor.name,
      defaultUnit: sensor.defaultUnit,
      possibleUnits: sensor.possibleUnits
    } as SensorDataDTO
  }
}
