import {Injectable} from "@angular/core";
import {ConfigDataDTO} from "../dtos/config-data.dto";
import {BehaviorSubject} from "rxjs";

@Injectable()
export class AvailableEndpointsService {

  private readonly configData = new BehaviorSubject<ConfigDataDTO>(undefined);

  readonly configData$ = this.configData.asObservable();

  constructor() {
  }

  setConfigData(configData: ConfigDataDTO) {
    this.configData.next(configData);
  }
}
