import {Component} from '@angular/core';
import {PageType} from "../shared/enum/page-type.enum";
import {MatDialog} from "@angular/material/dialog";
import {FileUploadDialogComponent} from "./file-upload/file-upload-dialog.component";

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent {
  readonly pageOptions = PageType;
  readonly CATCH_PHRASE = "Draw & compare";
  readonly DESCRIPTION = "Data Visualiser for DataHub is an intuitive visualization tool designed for DataHub" +
    " users that lets them show their data on beautiful charts and tables";
  readonly UPLOAD_BUTTON_LABEL = "Upload configuration file";

  constructor(public readonly dialog: MatDialog) {
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(FileUploadDialogComponent, {
      disableClose: true
    });
  }
}
