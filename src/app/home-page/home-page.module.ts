import {NgModule} from '@angular/core';
import {RouterModule} from "@angular/router";
import {HomePageComponent} from "./home-page.component";
import {SharedModule} from "../shared/shared.module";
import {MatIconModule} from "@angular/material/icon";
import {MatButtonModule} from "@angular/material/button";
import {FileUploadDialogComponent} from "./file-upload/file-upload-dialog.component";
import {MatDialogModule} from "@angular/material/dialog";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {MatProgressBarModule} from "@angular/material/progress-bar";
import {HttpClientModule} from "@angular/common/http";
import {FileUploadService} from "./file-upload/services/file-upload.service";
import {ConfigDataMapper} from "./sevices/config-data-mapper.service";

@NgModule({
  declarations: [HomePageComponent, FileUploadDialogComponent],
  imports: [
    RouterModule,
    SharedModule,
    MatIconModule,
    MatButtonModule,
    MatDialogModule,
    BrowserAnimationsModule,
    MatProgressBarModule,
    HttpClientModule
  ],
  providers: [FileUploadService, ConfigDataMapper, FileReader],
  exports: [HomePageComponent, FileUploadDialogComponent]
})
export class HomePageModule {
}
