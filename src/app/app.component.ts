import {Component} from '@angular/core';
import {ChartCounterService} from "./shared/services/chart-counter.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  constructor(private readonly chartCounterService: ChartCounterService) {
  }
}
