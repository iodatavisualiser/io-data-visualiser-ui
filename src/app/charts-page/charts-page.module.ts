import {NgModule} from '@angular/core';

import {RouterModule} from "@angular/router";
import {ChartsDataMapper} from "./services/chart-data-mapper.service";
import {TimeChartComponent} from "./charts/time-chart/time-chart.component";
import {HighchartsChartModule} from "highcharts-angular";
import {ChartsPageComponent} from "./charts-page-component";
import {SharedModule} from "../shared/shared.module";
import {AvailableChartsService} from "./services/available-charts.service";
import {MatListModule} from "@angular/material/list";
import {MatIconModule} from "@angular/material/icon";
import {CommonModule} from "@angular/common";
import {MatButtonModule} from "@angular/material/button";
import {ChartDataRepository} from "./services/chart-data.repository";
import {MatTooltipModule} from "@angular/material/tooltip";
import {DataConverterService} from "../recent-datasets/services/data-converter.service";
import {MatProgressSpinnerModule} from "@angular/material/progress-spinner";
import {ChartCounterService} from "../shared/services/chart-counter.service";

@NgModule({
  declarations: [
    TimeChartComponent,
    ChartsPageComponent
  ]
  ,
  imports: [
    RouterModule,
    HighchartsChartModule,
    SharedModule,
    MatListModule,
    MatIconModule,
    CommonModule,
    MatButtonModule,
    MatTooltipModule,
    MatProgressSpinnerModule
  ],
  providers: [
    ChartsDataMapper,
    AvailableChartsService,
    ChartDataRepository,
    DataConverterService,
    ChartCounterService
  ],
  exports: [
    TimeChartComponent,
    ChartsPageComponent
  ]
})
export class ChartsPageModule {
}
