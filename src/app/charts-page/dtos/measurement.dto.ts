export class MeasurementDTO {
  readonly timestamp: Date;
  value: any;

  constructor(value: number, date: Date) {
    this.value = value;
    this.timestamp = date
  }
}
