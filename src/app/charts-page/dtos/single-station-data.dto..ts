import {MeasurementDTO} from "./measurement.dto";
import {pmUnits, pressureUnits, temperatureUnits} from "../../shared/units";

export class SingleStationDataDTO {
  readonly id: string;
  readonly from: Date;
  readonly to: Date;
  readonly type: string;
  unit: any;
  data: MeasurementDTO[];

  constructor(id: string, from: Date, to: Date, type: string, unit: temperatureUnits | pressureUnits | pmUnits | any, data: MeasurementDTO[]) {
    this.id = id;
    this.from = from;
    this.to = to;
    this.type = type;
    this.unit = unit;
    this.data = data;
  }
}
