import {SingleStationDataDTO} from "./single-station-data.dto.";

export interface MultipleStationsDataDTO {
  readonly from: Date;
  readonly to: Date;
  readonly type: string;
  unit: string;
  readonly stations: SingleStationDataDTO[];
}
