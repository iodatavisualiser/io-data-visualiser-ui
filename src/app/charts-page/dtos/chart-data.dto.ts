import {MultipleStationsDataDTO} from "./multiple-stations-data.dto";

export interface ChartDataDTO {
  readonly title: string,
  readonly multipleStationsData: MultipleStationsDataDTO
}
