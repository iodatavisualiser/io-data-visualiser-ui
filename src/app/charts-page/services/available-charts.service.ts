import {Injectable} from "@angular/core";
import {BehaviorSubject} from "rxjs";
import {ChartDataDTO} from "../dtos/chart-data.dto";

@Injectable()
export class AvailableChartsService {

  private readonly chartData = new BehaviorSubject<ChartDataDTO[]>([]);

  readonly chartData$ = this.chartData.asObservable();

  constructor() {
  }

  addChartData(chartData: ChartDataDTO): void {
    const currentChartData = this.chartData.value;
    this.chartData.next(currentChartData.concat(chartData));
  }
}
