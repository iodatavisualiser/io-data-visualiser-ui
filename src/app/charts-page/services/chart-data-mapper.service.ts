import {Injectable} from "@angular/core";
import {SingleStationDataDTO} from "../dtos/single-station-data.dto.";
import {MeasurementDTO} from "../dtos/measurement.dto";
import {ConfigDataDTO} from "../../home-page/dtos/config-data.dto";
import {DataConverterService} from "../../recent-datasets/services/data-converter.service";

@Injectable()
export class ChartsDataMapper {

  constructor(private readonly dataConverterService: DataConverterService) {
  }

  mapToSingleStationDTO(station: any, configData: ConfigDataDTO, targetUnit: string): SingleStationDataDTO {

    const defaultUnit = configData.sensors.filter(sensor => sensor.name === station.type)[0].defaultUnit;

    const measurements: MeasurementDTO[] = station.data.map((item: { date: any; value: any; }) => {
        return {
          timestamp: new Date(item.date),
          value: item.value
        } as MeasurementDTO;
      }
    );

    if (targetUnit !== defaultUnit) {
      measurements.map(measurement => this.dataConverterService.convertUnitOnChartData(measurement, defaultUnit, targetUnit));
    }

    measurements.sort((a: MeasurementDTO, b: MeasurementDTO) => b.timestamp.getTime() - a.timestamp.getTime());
    return {
      id: station.id,
      from: new Date(station.from),
      to: new Date(station.to),
      type: station.type,
      unit: station.unit,
      data: measurements
    } as SingleStationDataDTO;
  }
}
