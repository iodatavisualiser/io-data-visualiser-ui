import {Injectable} from "@angular/core";
import {EndpointDTO} from "../../shared/dtos/endpoint.dto";
import {HttpClient, HttpParams} from "@angular/common/http";
import {ConfigDataDTO} from "../../home-page/dtos/config-data.dto";
import {ChartsDataMapper} from "./chart-data-mapper.service";
import {forkJoin, map, Observable} from "rxjs";
import {SingleStationDataDTO} from "../dtos/single-station-data.dto.";
import {formatDate, registerLocaleData} from "@angular/common";
import localePl from '@angular/common/locales/pl';

@Injectable()
export class ChartDataRepository {
  constructor(private readonly http: HttpClient,
              private readonly chartDataMapper: ChartsDataMapper) {
    registerLocaleData(localePl, "pl-PL");
  }

  readonly chartDataEndpoint = "http://localhost:8080/response";

  getMultipleStationsData(endpoints: EndpointDTO[], from: Date, to: Date, value: string, configData: ConfigDataDTO, targetUnit: string): Observable<SingleStationDataDTO[]> {
    let queryParams = new HttpParams();
    queryParams = queryParams.append("from", formatDate(from, "yyyy-MM-dd'T'HH:mm:ss'Z'", "pl_PL"));
    queryParams = queryParams.append("to", formatDate(to, "yyyy-MM-dd'T'HH:mm:ss'Z'", "pl-PL"));
    queryParams = queryParams.append("param", value);
    const singleStationsData = endpoints.map(endpoint => this.getSingleStationData(endpoint, configData, queryParams, targetUnit));
    return forkJoin(singleStationsData).pipe(map(responses => {
      return responses;
    }))
  }

  private getSingleStationData(endpoint: EndpointDTO, configData: ConfigDataDTO, queryParams: HttpParams, targetUnit: string): Observable<SingleStationDataDTO> {
    const url = configData.stations.filter(station => station.name == endpoint?.id)[0]?.endpoint;
    queryParams = queryParams.append("url", url);
    return this.http.get(this.chartDataEndpoint, {params: queryParams}).pipe(map(response => {
      return this.chartDataMapper.mapToSingleStationDTO(response, configData, targetUnit);
    }));
  }
}
