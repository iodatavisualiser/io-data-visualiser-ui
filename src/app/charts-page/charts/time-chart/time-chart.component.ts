import {ChangeDetectionStrategy, Component, Input, OnInit} from "@angular/core";
import {MultipleStationsDataDTO} from "../../dtos/multiple-stations-data.dto";
import * as Highcharts from 'highcharts';
import HC_exporting from 'highcharts/modules/exporting';
import HC_exportData from 'highcharts/modules/export-data';
import {DataNameParserService} from "../../../shared/services/data-name-parser.service";

HC_exporting(Highcharts);
HC_exportData(Highcharts);

@Component({
  selector: 'app-time-chart',
  templateUrl: './time-chart.component.html',
  styleUrls: ['./time-chart.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TimeChartComponent implements OnInit {

  @Input()
  _data: MultipleStationsDataDTO;
  @Input('data')
  set data(data: MultipleStationsDataDTO) {
    this._data = data
  }

  get data() {
    return this._data
  }

  @Input()
  style: string

  Highcharts: typeof Highcharts = Highcharts;
  chartOptions: Highcharts.Options;

  constructor(readonly nameParserService: DataNameParserService) {
  }

  ngOnInit() {
    const chartTitle = this.data?.stations.length > 1 ? `Stations comparision \\ ${this.data.type}` : `${this.data.stations[0].id} \\ ${this.data.type}`;
    this.chartOptions = {
      title: {
        text: chartTitle
      },
      xAxis: {
        title: {
          text: 'Timestamp'
        },
        type: 'datetime'
      },
      yAxis: {
        title: {
          text: `${this.nameParserService.parseName(this.data?.type)} [${this.data?.unit}]`
        }
      },
      tooltip: {
        headerFormat: '{point.x:%e %b %H:%M:%S}'
      },
      series: this.data?.stations.map(station => {
        return {
          type: 'line',
          data: station.data.map(item => [item.timestamp.getTime(), Number(Number(item.value).toFixed(2))]),
          name: station.id
        }
      }),
      chart: {
        styledMode: true
      }
    };
  }
}
