import {Component, OnInit} from "@angular/core";
import {PageType} from "../shared/enum/page-type.enum";
import {AvailableChartsService} from "./services/available-charts.service";
import {MultipleStationsDataDTO} from "./dtos/multiple-stations-data.dto";
import {MatListOption} from "@angular/material/list";
import {ChartDataDTO} from "./dtos/chart-data.dto";
import {HttpErrorResponse, HttpStatusCode} from "@angular/common/http";
import {catchError, throwError} from "rxjs";
import {ChartCounterService} from "../shared/services/chart-counter.service";

@Component({
  selector: 'app-charts-page',
  templateUrl: './charts-page.component.html',
  styleUrls: ['./chart-page.component.scss']
})
export class ChartsPageComponent implements OnInit {
  readonly pageOptions = PageType;

  charts: ChartDataDTO[];
  error: HttpErrorResponse;
  noCharts = true;
  selections: MultipleStationsDataDTO[];
  chartsLoading: boolean = true;
  readonly noDataText = "OOPS...!\n" +
    "Looks like you haven't created any charts yet."
  errorMessage: string;
  readonly goToMyDatasets = "Go to my datasets";
  readonly listSubHeader = "Saved charts";

  constructor(private readonly availableChartService: AvailableChartsService,
              private readonly chartCounterService: ChartCounterService) {
  }

  ngOnInit() {
    this.initializeCharts();
  }

  private initializeCharts() {
    this.chartsLoading = true;
    this.availableChartService.chartData$.pipe(
      catchError(err => {
        this.error = err;
        this.setErrorMessage(this.error.status);
        return throwError(err);
      })
    )
      .subscribe({
          next: (chartData) => {
            this.charts = chartData;
            this.noCharts = this.charts.length === 0;
            if (this.chartCounterService.getCounter() === this.charts.length) {
              this.chartsLoading = false;
            }
          }
        }
      );
  }

  showDates(chart: MultipleStationsDataDTO) {
    return `${chart.from.toLocaleString()} - ${chart.to.toLocaleString()}`;
  }

  updateSelections(options: MatListOption[]) {
    this.selections = options.map(option => option.value.multipleStationsData);
  }

  private setErrorMessage(status: HttpStatusCode): void {
    if (status === HttpStatusCode.InternalServerError) {
      this.errorMessage = "Sorry, an error has ocurred during data processing. Try again later."
    } else if (status === HttpStatusCode.ServiceUnavailable) {
      this.errorMessage = "Sorry, DataHub server is currently unavailable. Try again later."
    } else if (status === HttpStatusCode.NoContent) {
      this.errorMessage = "Sorry, there is currently no available data based on your criteria."
    } else {
      this.errorMessage = "Sorry, an error has ocurred. Try again later."
    }
  }
}
